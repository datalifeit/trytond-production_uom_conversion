from trytond.pool import Pool
from .production import *


def register():
    Pool.register(
        Production,
        module='production_uom_conversion', type_='model')